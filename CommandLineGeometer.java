import java.util.Date;
import java.util.Scanner;
import java.lang.Math;
import java.text.DecimalFormat;

public class CommandLineGeometer {

    private enum GeometricShape { sphere, triangle, cylinder };

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        DecimalFormat fmt = new DecimalFormat("0.####");		//Creates a format framework for proper decimal display
	GeometricShape shape = GeometricShape.sphere;
        double radius;
        double height;
        int x , y, z;							//Simplified int declaration


        System.out.println("Carson Quigley & Dylan Bish " + new Date());
        System.out.println("Welcome to the Command Line Geometer!");
        System.out.println();

        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();				//takes user input for radius of sphere
        System.out.println();

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double sphereVolume = GeometricCalculator.calculateSphereVolume(radius);	//Passes radius variable to Geometric Calc. class
        System.out.println("The volume is equal to " + fmt.format(sphereVolume));	//Displays and formats output

	double sphereSA = GeometricCalculator.calculateSphereSurfacearea(radius);	//passes radius to Calc. for surface area
	System.out.println("The surface area is equal to " + fmt.format(sphereSA));
	System.out.println();
        shape = GeometricShape.triangle;			//Changes shape identifier to triangle

        System.out.println("What is the length of the first side?");
        x = scan.nextInt();

        System.out.println("What is the length of the second side?");		//accepts user input for triangle sides
        y = scan.nextInt();

        System.out.println("What is the length of the third side?");
        z = scan.nextInt();
        System.out.println();

        System.out.println("Calculating the area of a " + shape);
        double triangleArea = GeometricCalculator.calculateTriangleArea(x, y, z);	//passes triangle sides to calc.
        System.out.println("The area is equal to " + fmt.format(triangleArea));
        System.out.println();

        shape = GeometricShape.cylinder;		//changes shape identifier to cylinder

        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();		//accepts user input for cylinder radius
        System.out.println();

        System.out.println("What is the height for the " + shape + "?");
        height = scan.nextDouble();		//accepts user input for cylinder height
        System.out.println();

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double cylinderVolume = GeometricCalculator.calculateCylinderVolume(radius, height);	//passes height and radius to calc for volume calculation
        System.out.println("The volume is equal to " + fmt.format(cylinderVolume));
	double cylinderSA = GeometricCalculator.calculateCylinderSurfacearea(radius, height);	//passes radius and height to calc for surface area calculation
	System.out.println("The surface area is equal to " + fmt.format(cylinderSA));
        System.out.println();
    }
}
